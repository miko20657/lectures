# Лекция: Создание RESTful API с использованием Laravel

Сегодня мы подробно рассмотрим, что такое RESTful API, как его создавать и использовать с Laravel, и как обеспечить его тестирование.

## Введение в RESTful API

API (Application Programming Interface) — это код, который позволяет двум приложениям обмениваться данными с сервера. На русском языке его принято называть программным интерфейсом приложения.

REST (Representational State Transfer) — это способ создания API с помощью протокола HTTP. На русском его называют «передачей состояния представления».

Технологию REST API применяют везде, где пользователю сайта или веб-приложения нужно предоставить данные с сервера. Например, при нажатии иконки с видео на видеохостинге REST API проводит операции и запускает ролик с сервера в браузере. В настоящее время это самый распространенный способ организации API. Он вытеснил ранее популярные способы SOAP и WSDL.

### Что такое REST?

REST (Representational State Transfer) - это архитектурный стиль для разработки веб-сервисов. Он использует стандартные HTTP методы и принципы для создания и управления ресурсами, представленными в формате JSON или XML. Основные принципы REST включают:

1. **Статeless (Бездефицитный)**: Каждый запрос от клиента к серверу должен содержать всю необходимую информацию для понимания и обработки запроса.
2. **Client-Server (Клиент-Сервер)**: Клиент и сервер должны быть независимыми друг от друга, что позволяет развивать их независимо.
3. **Cacheable (Кэшируемый)**: Ответы на запросы должны быть помечены как кэшируемые или нет для повышения производительности.
4. **Uniform Interface (Единый интерфейс)**: Определяет взаимодействие между клиентом и сервером, что упрощает и стандартизирует архитектуру.
5. **Layered System (Слоистая система)**: Архитектура может состоять из нескольких слоев, что позволяет использовать прокси, шлюзы и другие промежуточные серверы.


Это отличает REST API от метода простого протокола доступа к объектам SOAP (Simple Object Access Protocol), созданного Microsoft в 1998 году. В SOAP взаимодействие по каждому протоколу нужно прописывать отдельно только в формате XML. Также в SOAP нет кэшируемости запросов, более объемная документация и реализация словаря, отдельного от HTTP. Это делает стиль REST API более легким в реализации, чем стандарт SOAP.

Несмотря на отсутствие стандартов, при создании REST API есть общепринятые лучшие практики, например:

использование защищенного протокола HTTPS
использование инструментов для разработки API Blueprint и Swagger
применение приложения для тестирования Get Postman
применение как можно большего количества HTTP-кодов (список)

### Преимущества RESTful API

RESTful API имеет множество преимуществ:

Простота использования: RESTful API использует стандартные HTTP методы и форматы данных, что делает его легким в использовании.
Масштабируемость: RESTful API позволяет масштабировать приложение горизонтально и вертикально.
Гибкость: RESTful API позволяет создавать и управлять ресурсами с помощью различных методов HTTP.
Надежность: RESTful API обеспечивает надежную передачу данных между клиентом и сервером.
Безопасность: RESTful API обеспечивает безопасность данных с помощью HTTPS и других мер защиты.


### HTTP Методы

RESTful API использует стандартные HTTP методы для выполнения операций CRUD:

- **GET**: Получение ресурса или списка ресурсов.
- **POST**: Создание нового ресурса.
- **PUT**: Обновление существующего ресурса.
- **DELETE**: Удаление ресурса.

### Маршруты в Laravel

Маршруты для RESTful API обычно определяются в файле `routes/api.php`.

Пример:

```php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ArticleController;

Route::middleware('auth:api')->group(function () {
    Route::get('/articles', [ArticleController::class, 'index']);
    Route::post('/articles', [ArticleController::class, 'store']);
    Route::get('/articles/{article}', [ArticleController::class, 'show']);
    Route::put('/articles/{article}', [ArticleController::class, 'update']);
    Route::delete('/articles/{article}', [ArticleController::class, 'destroy']);
});
```

## Часть 1: Создание RESTful API с использованием Laravel

### Создание контроллера

Для начала создадим контроллер для управления ресурсами "Статьи":

```sh
php artisan make:controller Api/ArticleController --api
```

Этот контроллер будет содержать методы для выполнения операций CRUD.

Файл: `app/Http/Controllers/Api/ArticleController.php`

```php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    public function index()
    {
        return Article::all();
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

        return Article::create($request->all());
    }

    public function show(Article $article)
    {
        return $article;
    }

    public function update(Request $request, Article $article)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

        $article->update($request->all());

        return $article;
    }

    public function destroy(Article $article)
    {
        $article->delete();

        return response()->json(['message' => 'Article deleted successfully']);
    }
}
```

### Модель и миграция

Создадим модель и миграцию для ресурса "Статья":

```sh
php artisan make:model Article -m
```

Файл миграции: `database/migrations/YYYY_MM_DD_create_articles_table.php`

```php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('content');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
```

Запустим миграцию:

```sh
php artisan migrate
```

Файл модели: `app/Models/Article.php`

```php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'content'];
}
```

### Аутентификация API

Laravel предлагает встроенную поддержку аутентификации для API через пакет Laravel Sanctum. Установим Sanctum:

```sh
composer require laravel/sanctum
```

Опубликуем конфигурацию и запустим миграции:

```sh
php artisan vendor:publish --provider="Laravel\Sanctum\SanctumServiceProvider"
php artisan migrate
```

Добавим Middleware Sanctum в `api` группу в файле `app/Http/Kernel.php`:

```php
'api' => [
    \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
    'throttle:api',
    \Illuminate\Routing\Middleware\SubstituteBindings::class,
],
```

Настроим модель `User` для использования токенов API:

```php
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    // ...
}
```

### Вход и регистрация пользователей

Добавим маршруты и контроллеры для регистрации и входа пользователей:

Файл: `routes/api.php`

```php
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
```

Создадим контроллер `AuthController`:

```sh
php artisan make:controller Api/AuthController
```

Файл: `app/Http/Controllers/Api/AuthController.php`

```php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json(['message' => 'User registered successfully']);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return response()->json([
            'token' => $user->createToken('api-token')->plainTextToken,
        ]);
    }
}
```
