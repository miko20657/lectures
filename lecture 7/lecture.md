# Лекция по Laravel: Использование Composer

## Введение

Composer — это менеджер зависимостей для PHP, который позволяет управлять библиотеками и пакетами, необходимыми для вашего проекта. В Laravel Composer играет ключевую роль, обеспечивая установку и обновление всех компонентов, необходимых для работы фреймворка и дополнительных библиотек.

## Что такое Composer?

Composer — это инструмент для управления зависимостями, который автоматически устанавливает нужные версии библиотек, учитывая зависимости каждого компонента вашего проекта. Это делает процесс разработки более управляемым и упрощает обновление библиотек.

## Установка Composer

Перед началом работы с Laravel необходимо установить Composer. Процесс установки зависит от вашей операционной системы.

### Установка на Windows

1. Скачайте Composer-Setup.exe с официального сайта [getcomposer.org](https://getcomposer.org).
2. Запустите установщик и следуйте инструкциям.

### Установка на MacOS и Linux

Для MacOS и Linux используйте следующую команду в терминале:

```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer
```

После установки проверьте успешность установки командой:

```bash
composer --version
```

## Использование Composer с Laravel

Laravel тесно интегрирован с Composer и использует его для управления всеми зависимостями.

### Создание нового проекта Laravel

Для создания нового проекта Laravel используйте следующую команду:

```bash
composer create-project --prefer-dist laravel/laravel имя_проекта
```

Эта команда создаст новый проект с последней версией Laravel.

### Файл composer.json

Файл `composer.json` находится в корне вашего проекта и содержит список всех зависимостей вашего проекта.

Пример файла `composer.json`:

```json
{
    "name": "laravel/laravel",
    "type": "project",
    "require": {
        "php": "^7.3|^8.0",
        "fideloper/proxy": "^4.2",
        "fruitcake/laravel-cors": "^2.0",
        "guzzlehttp/guzzle": "^7.0.1",
        "laravel/framework": "^8.12",
        "laravel/tinker": "^2.5"
    },
    "require-dev": {
        "facade/ignition": "^2.5",
        "fzaninotto/faker": "^1.9.1",
        "mockery/mockery": "^1.4.2",
        "nunomaduro/collision": "^5.0",
        "phpunit/phpunit": "^9.3.3"
    },
    "autoload": {
        "psr-4": {
            "App\\": "app/"
        },
        "classmap": [
            "database/seeds",
            "database/factories"
        ],
        "files": [
            "app/Helpers/helpers.php"
        ]
    },
    "scripts": {
        "post-autoload-dump": [
            "Illuminate\\Foundation\\ComposerScripts::postAutoloadDump",
            "@php artisan package:discover --ansi"
        ],
        "post-root-package-install": [
            "@php -r \"file_exists('.env') || copy('.env.example', '.env');\""
        ],
        "post-create-project-cmd": [
            "@php artisan key:generate --ansi"
        ]
    },
    "config": {
        "optimize-autoloader": true,
        "preferred-install": "dist",
        "sort-packages": true
    },
    "minimum-stability": "dev",
    "prefer-stable": true
}
```

### Основные команды Composer

#### Установка зависимостей

```bash
composer install
```

Эта команда устанавливает все зависимости, перечисленные в `composer.json`.

#### Обновление зависимостей

```bash
composer update
```

Эта команда обновляет все зависимости до их последних версий в пределах указанных в `composer.json`.

#### Добавление новой зависимости

Чтобы добавить новую библиотеку, используйте команду `require`:

```bash
composer require имя_пакета
```

#### Удаление зависимости

Для удаления зависимости используйте команду `remove`:

```bash
composer remove имя_пакета
```

## Использование Composer для автозагрузки

Composer автоматически генерирует файл автозагрузки, который позволяет вам использовать классы без необходимости явного подключения файлов.

### Автозагрузка классов

В `composer.json` укажите директории для автозагрузки:

```json
"autoload": {
    "psr-4": {
        "App\\": "app/"
    }
}
```

После изменения файла `composer.json` выполните команду:

```bash
composer dump-autoload
```

Эта команда обновит файл автозагрузки.


## Зачем нужен файл composer.lock

Файл `composer.lock` играет важную роль в управлении зависимостями вашего PHP проекта, особенно при работе с фреймворком Laravel. Давайте рассмотрим, для чего именно он нужен и как влияет на работу с зависимостями.

### Основные функции файла composer.lock

1. **Фиксация версий зависимостей**:
    - Когда вы запускаете команду `composer install`, Composer использует файл `composer.lock` для установки точно тех версий библиотек, которые указаны в этом файле. Это обеспечивает стабильность и предсказуемость зависимостей вашего проекта.
    - Файл `composer.lock` фиксирует точные версии всех пакетов и их зависимостей, которые были установлены при последнем запуске `composer update`.

2. **Совместная работа над проектом**:
    - В команде разработчиков наличие `composer.lock` позволяет всем участникам использовать одинаковые версии библиотек, что предотвращает проблемы, связанные с несовместимостью версий.
    - Когда новый разработчик клонирует репозиторий и запускает `composer install`, он получит те же версии зависимостей, что и остальные члены команды.

3. **Ускорение установки зависимостей**:
    - Использование файла `composer.lock` значительно ускоряет процесс установки зависимостей, поскольку Composer не нужно разрешать зависимости каждый раз, а просто устанавливает те версии, которые зафиксированы в `composer.lock`.

### Разница между composer.json и composer.lock

- **composer.json**: Этот файл содержит список всех зависимостей вашего проекта и их желаемые версии. Здесь вы указываете, какие пакеты вам нужны и какие версии вы предпочитаете (например, `"laravel/framework": "^8.0"`).

- **composer.lock**: Этот файл фиксирует конкретные версии пакетов, которые были установлены, а также все их зависимости. Он создается или обновляется автоматически при выполнении команды `composer update` и используется при выполнении команды `composer install`.

### Работа с composer.lock

- **composer install**:
    - Использует `composer.lock` для установки зависимостей. Если `composer.lock` существует, Composer установит именно те версии пакетов, которые зафиксированы в этом файле.
    - Если `composer.lock` отсутствует, Composer создаст его на основе зависимостей, указанных в `composer.json`.

- **composer update**:
    - Обновляет все зависимости до последних версий в рамках ограничений, указанных в `composer.json`, и обновляет файл `composer.lock` с новыми версиями пакетов.

### Почему важно коммитить composer.lock в систему контроля версий

1. **Стабильность проекта**: Все разработчики будут использовать одни и те же версии пакетов, что снижает риск возникновения неожиданных ошибок и проблем с совместимостью.

2. **Легкость деплоя**: В продакшен-среде вы можете быть уверены, что установлены точно те же версии пакетов, что и в тестовой или разработческой среде.

3. **Повторяемость сборки**: Любой, кто клонирует ваш репозиторий, сможет собрать проект с точно такими же зависимостями, что и у вас.

### Заключение

Файл `composer.lock` является важной частью управления зависимостями в проектах на PHP, обеспечивая стабильность, предсказуемость и совместимость версий пакетов. Его использование и хранение в системе контроля версий помогает избежать множества потенциальных проблем и упрощает совместную работу над проектом.


## Заключение

Composer является незаменимым инструментом для работы с Laravel, обеспечивая удобное управление зависимостями и автозагрузку классов. Понимание и использование Composer облегчит процесс разработки и поддержки вашего проекта.




### Интеграция Swagger в Laravel с помощью L5-Swagger

L5-Swagger — это популярный пакет для интеграции Swagger с Laravel, который автоматически генерирует документацию на основе ваших маршрутов и контроллеров.

#### Установка

1. Установите пакет L5-Swagger через Composer:
    ```bash
    composer require "darkaonline/l5-swagger"
    ```

2. Опубликуйте конфигурационный файл пакета:
    ```bash
    php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
    ```

#### Конфигурация

Конфигурационный файл `l5-swagger.php` будет опубликован в директорию `config`. В этом файле вы можете настроить различные параметры генерации документации.

Пример конфигурации:

```php
return [
    'default' => 'default',
    'documentations' => [
        'default' => [
            'api' => [
                'title' => 'L5 Swagger UI',
            ],

            'routes' => [
                'api' => 'api/documentation',
                'docs' => 'docs',
            ],

            'paths' => [
                'docs' => storage_path('api-docs'),
                'annotations' => [
                    base_path('app'),
                ],
            ],
        ],
    ],
];
```

#### Использование

1. Добавьте аннотации к вашим контроллерам и методам. Например:

    ```php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="L5 Swagger API",
     *      description="L5 Swagger OpenApi description",
     *      @OA\Contact(
     *          email="your-email@example.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     */
    class UserController extends Controller
    {
        /**
         * @OA\Get(
         *     path="/api/users",
         *     @OA\Response(response="200", description="Display a listing of users.")
         * )
         */
        public function index()
        {
            return User::all();
        }
    }
    ```

2. Сгенерируйте документацию:
    ```bash
    php artisan l5-swagger:generate
    ```

3. Откройте URL `http://your-app-url/api/documentation`, чтобы просмотреть сгенерированную документацию Swagger.

#### Пример использования аннотаций

Аннотации используются для описания маршрутов, параметров, ответов и других элементов API.

Пример контроллера с аннотациями:

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/users",
     *     summary="Get list of users",
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/User")
     *         )
     *     )
     * )
     */
    public function index()
    {
        return User::all();
    }

    /**
     * @OA\Post(
     *     path="/api/users",
     *     summary="Create a new user",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="User created",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     )
     * )
     */
    public function store(Request $request)
    {
        $user = User::create($request->all());
        return response()->json($user, 201);
    }
}

/**
 * @OA\Schema(
 *     schema="User",
 *     type="object",
 *     title="User",
 *     required={"name", "email", "password"},
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         format="int64",
 *         description="ID"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="Name"
 *     ),
 *     @OA\Property(
 *         property="email",
 *         type="string",
 *         format="email",
 *         description="Email"
 *     ),
 *     @OA\Property(
 *         property="password",
 *         type="string",
 *         format="password",
 *         description="Password"
 *     ),
 *     @OA\Property(
 *         property="created_at",
 *         type="string",
 *         format="date-time",
 *         description="Creation date"
 *     ),
 *     @OA\Property(
 *         property="updated_at",
 *         type="string",
 *         format="date-time",
 *         description="Update date"
 *     )
 * )
 */
class User extends Model
{
    protected $fillable = ['name', 'email', 'password'];
}
```

### Заключение

L5-Swagger — это мощный инструмент для создания и поддержания документации API в Laravel. Он позволяет автоматически генерировать документацию на основе аннотаций в вашем коде, что упрощает процесс создания и поддержания актуальной документации. Следуя приведенным выше шагам, вы сможете легко интегрировать Swagger в ваше Laravel-приложение и обеспечить качественную документацию для вашего API.