Лекции по курсу PHP Laravel
===========================
Лекции с примерами в папках `lecture1`, `lecture2`, `lecture3` и т.д.

Домашние задания в папках `homework1`, `homework2`, `homework3` и т.д.

Первый проект - https://gitlab.com/taibek_kair/lecture-app

Второй проект - https://gitlab.com/taibek_kair/tauda-adaspa

