<?php
// 1. Дано число. Проверьте, отрицательное оно или нет. Выведите об этом информацию в консоль

$nums = [5,-3,9,11,0,-13];

foreach ($nums as $num){
    echo $num;
    if ($num<0){
        echo " :negative\t";
    } else {
        echo " :positive\t";
    }
}
echo "\n";


// 2. Дана строка. Выведите в консоль длину этой строки.
$str = 'abcdefg';

echo strlen($str)." length of '{$str}'\n";


// 3. Дана строка. Проверьте, есть ли в этой строке символ 'a'. Если есть, то выведите 'да', если нет, то 'нет'.
$strs = ['abc', 'defg', 'hg', 'a', 'sfda'];

foreach ($strs as $str){
    echo $str;
    if (str_contains($str,'a')){
        echo " :да   ";
    } else {
        echo " :нет   ";
    }
}
echo "\n";


// 4. Дано число. Проверьте, что оно делится на 2, на 3, на 5, на 6, на 9 без остатка.
//I took array $nums from above
foreach ($nums as $num){
    echo $num;
    if($num%2 === 0 || $num%3 === 0 || $num%5 === 0){
        echo " :Делиться\t";
    } else {
        echo " :Не делиться\t";
    }
}
echo "\n";


// 5. Дано число. Проверьте, что оно делится на 3, на 5, на 7, на 11 без остатка.
//I took array $nums from above
foreach ($nums as $num){
    echo $num;
    if($num%3 === 0 || $num%5 === 0 || $num%7 === 0 || $num%11 === 0){
        echo " :Делиться\t";
    } else {
        echo " :Не делиться\t";
    }
}
echo "\n";


//6. Дана строка. Выведите в консоль последний символ строки.
//I took array $str from above
echo "The last symbol of: '{$str}' - '{$str[-1]}'\n";
//7. Дана строка. Выведите в консоль последний символ строки.
echo "The last symbol of: '{$str}' - '{$str[-1]}'\n";


//8. Дан треугольник. Выведите в консоль его площадь.
//Формула Герона
$a = 3; $b = 4; $c = 5;
$p = ($a + $b + $c)/2;
$s = (int)sqrt($p*($p-$a)*($p-$b)*($p-$c));
echo "Area of triangle with sides 3,4,5: ".$s;


//9. Дан прямоугольник. Выведите в консоль его площадь.
// i used $b and $c from above
echo "Area of rectangle with sides 4,5,4,5: ".($b+$c)*2 ."\n";

//10. Дано число. Выведите в консоль квадрат этого числа.
// i used $a from above
echo pow($a,2);
