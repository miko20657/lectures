ООП 

Основные принципы ООП:
1. Инкапсуляция
2. Наследование
3. Полиморфизм
4. Абстракция

Класс - это шаблон для создания объектов. Объект - это экземпляр класса.

Классы в PHP объявляются с помощью ключевого слова `class`.

```php
class Cat {
    // свойства класса
    public $name;
    private $age;
    protected $color;

    // методы класса
    public function eat() {
        // тело метода
    }

    private function run() {
        // тело метода
    }

    protected function sleep() {
        // тело метода
    }
}
```

Свойства класса могут быть публичными, приватными или защищенными.

Методы класса могут быть публичными, приватными или защищенными.

Публичные методы и свойства доступны из любого места в коде.

Приватные методы и свойства доступны только внутри класса.

Защищенные методы и свойства доступны внутри класса и в классах-наследниках.

Создание объекта:

```php
$tom = new Cat();
$tom->name = "Tom";
$tom->age = 3;
$tom->color = "blue";

$garfield = new Cat();
$garfield->name = "Garfield";
$garfield->age = 5;
$garfield->color = "orange";

```

![img.png](img.png)


Доступ к свойствам и методам объекта:

```php  

$tom->name; //Tom
$tom->sleep();

$garfield->name; //Garfield
$garfield->sleep
```


Конструктор - это метод, который вызывается при создании объекта.

```php
class Cat {
    public function __construct($name, $age, $color) {
        // тело конструктора
        $this->name = $name;
        $this->age = $age;
        $this->color = $color;
    }
}

$tom = new Cat("Tom", 3, "blue");
$garfield = new Cat("Garfield", 5, "orange");

```

Деструктор - это метод, который вызывается при удалении объекта.

```php
class ClassName {
    public function __destruct() {
        // тело деструктора
    }
}
```

Наследование - это возможность создания нового класса на основе существующего.

```php
class Cat extends Animal {
    // тело класса
}
```

Полиморфизм - это возможность использования объектов с одинаковым интерфейсом без информации о конкретном типе.

Абст

```php
abstract class ClassName {
    // тело абстрактного класса
}
```

Интерфейс - это набор методов, которые должны быть реализованы в классе.

```php
interface InterfaceName {
    public function method1();
    public function method2();
}
```

Интерфейс может наследовать другие интерфейсы.

```php
interface InterfaceName extends Interface1, Interface2 {
    // тело интерфейса
}
```

Интерфейс может быть реализован классом.

```php
class ClassName implements InterfaceName {
    // тело класса
}
```

Интерфейс может быть реализован несколькими классами.

```php
class ClassName1 implements InterfaceName {
    // тело класса
}

class ClassName2 implements InterfaceName {
    // тело класса
}
```

Интерфейс может быть реализован абстрактным классом.

```php
abstract class AbstractClassName implements InterfaceName {
    // тело абстрактного класса
}
```

Интерфейс может быть реализован несколькими абстрактными классами.

```php

abstract class AbstractClassName1 implements InterfaceName {
    // тело абстрактного класса
}

abstract class AbstractClassName2 implements InterfaceName {
    // тело абстрактного класса
}
```

Класс может реализовывать несколько интерфейсов.

```php
class ClassName implements Interface1, Interface2 {
    // тело класса
}
```

Абстракция - это возможность скрыть детали реализации и предоставить только интерфейс.

Когда вы пишете программу, используя ООП, вы представляете её части через объекты реального мира. Но объекты
в программе не повторяют в точности их реальные аналоги,
да и это редко бывает нужно. Вместо этого объекты программы всего лишь моделируют свойства и поведения реальных
объектов, важные в конкретном контексте, а остальные —
игнорируют. Это и есть абстракция.


![img_1.png](img_1.png)


```php
abstract class ClassName {
    abstract public function method1();
    abstract public function method2();
}
```

Абстрактный класс - это класс, у которого есть хотя бы один абстрактный метод.

```php
abstract class ClassName {
    abstract public function method1();
}
```

Абстрактный класс не может быть создан.

```php
$object = new ClassName(); // Ошибка
```

Абстрактный класс может быть унаследован.

```php
class ChildClassName extends ClassName {
    // тело класса
}
```

Чем отличается абстрактный класс от интерфейса?

1. Абстрактный класс может содержать реализацию методов.
2. Класс может реализовывать несколько интерфейсов, но наследовать только один класс.
3. Интерфейс может быть реализован несколькими классами, а абстрактный класс - только одним.
4. Абстрактный класс может содержать конструктор и деструктор, а интерфейс - нет.
5. Абстрактный класс может содержать свойства, а интерфейс - нет.
6. Абстрактный класс может содержать статические методы, а интерфейс - нет.
7. Абстрактный класс может содержать защищенные методы и свойства, а интерфейс - нет.

Пример абстрактного класса:

```php
abstract class Shape {
    abstract public function getArea();
    abstract public function getPerimeter();
}

class Circle extends Shape {
    private $radius;

    public function __construct($radius) {
        $this->radius = $radius;
    }

    public function getArea() {
        return M_PI * $this->radius * $this->radius;
    }

    public function getPerimeter() {
        return 2 * M_PI * $this->radius;
    }
}

class Rectangle extends Shape {
    private $width;
    private $height;

    public function __construct($width, $height) {
        $this->width = $width;
        $this->height = $height;
    }

    public function getArea() {
        return $this->width * $this->height;
    }

    public function getPerimeter() {
        return 2 * ($this->width + $this->height);
    }
}

$circle = new Circle(5);
echo $circle->getArea(); // 78.54
echo $circle->getPerimeter(); // 31.42

$rectangle = new Rectangle(5, 10);
echo $rectangle->getArea(); // 50
echo $rectangle->getPerimeter(); // 30
```

Пример интерфейса:

```php
interface Shape {
    public function getArea();
    public function getPerimeter();
}

class Circle implements Shape {
    private $radius;

    public function __construct($radius) {
        $this->radius = $radius;
    }

    public function getArea() {
        return M_PI * $this->radius * $this->radius;
    }

    public function getPerimeter() {
        return 2 * M_PI * $this->radius;
    }
}

class Rectangle implements Shape {
    private $width;
    private $height;

    public function __construct($width, $height) {
        $this->width = $width;
        $this->height = $height;
    }

    public function getArea() {
        return $this->width * $this->height;
    }

    public function getPerimeter() {
        return 2 * ($this->width + $this->height);
    }
}

$circle = new Circle(5);
echo $circle->getArea(); // 78.54
echo $circle->getPerimeter(); // 31.42

$rectangle = new Rectangle(5, 10);

echo $rectangle->getArea(); // 50
echo $rectangle->getPerimeter(); // 30
```


Трейты - это механизм, который позволяет повторно использовать методы в разных классах.

Реальный пример использования трейтов:

```php
trait PrintClass
{
    public function printClassName()
    {
        echo get_class($this);
    }
}

trait Trait1 {
    // тело трейта
}

class Cat
{
    use PrintClass;
    
    public $name;
    public function __construct($name)
    {
        $this->name = $name;
    }
}

class Delivery
{
    use PrintClass;
    
    public $address;
    public function __construct($address)
    {
        $this->address = $address;
    }
}

$cat = new Cat("Tom");
$cat->printClassName(); // Cat
```

Класс может использовать несколько трейтов.

```php

