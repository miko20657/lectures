<?php

namespace lecture_4;

class ChildClass extends ParentClass
{
    use ExampleTrait;
}

$child = new ChildClass();
echo $child->getPrintClass(); // lecture_4\ChildClass