<?php

namespace lecture_4;


interface getTransport
{
    public function getTransport();
}

interface Fly
{
    public function getDelivery();
}

abstract class Delivery
{
    public $cost;
    public $weight;
    public function calculateCost()
    {
        return 0;
    }
}

class PostDelivery extends Delivery implements getTransport, Fly
{
    use ExampleTrait;
    public function calculateCost()
    {
        return 5;
    }

    public function getDelivery(){
        return 'Post';
    }

    public function getTransport(){
        return 'Train';
    }
}

class CourierDelivery extends Delivery
{
    public function calculateCost()
    {
        return 10;
    }
}

class AirDelivery extends Delivery
{
    public $range = 100;
    public function calculateCost()
    {
        return $this->range * 0.2 + 10;
    }
}

class UserD
{
    public $delivery;
    public function getDeliveryCost(Delivery $delivery)
    {
        return $delivery->calculateCost();
    }

}



$user = new UserD();
$user->delivery = new PostDelivery();
echo $user->getDeliveryCost($user->delivery); // 5


$user->delivery = new CourierDelivery();
echo $user->getDeliveryCost($user->delivery); // 10


$user->delivery = new AirDelivery();
echo $user->getDeliveryCost($user->delivery); // 20
