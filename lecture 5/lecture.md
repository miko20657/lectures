# Лекция: Blade Шаблоны в Laravel и Создание Шаблона для CRUD

## Введение

Blade - это встроенный шаблонизатор в Laravel, который позволяет удобно и эффективно создавать шаблоны для веб-приложений. Он предлагает множество полезных функций, таких как директивы, наследование шаблонов и компоненты, которые значительно упрощают процесс разработки.

В этой лекции мы рассмотрим основы Blade и создадим шаблон для операций CRUD (создание, чтение, обновление и удаление).

## Основы Blade

### 1. Директивы Blade

Директивы Blade - это специальные конструкции, которые упрощают написание логики в шаблонах. Вот несколько основных директив:

- `@extends` - указывает, что шаблон наследуется от другого шаблона.
- `@section` - определяет секцию содержимого, которую можно заполнить в дочерних шаблонах.
- `@yield` - определяет место для содержимого, которое будет заполнено дочерними шаблонами.
- `@include` - вставляет содержимое другого шаблона.
- `@if`, `@elseif`, `@else`, `@endif` - директивы для условий.
- `@foreach`, `@forelse`, `@endforelse`, `@endfor` - директивы для циклов.

### 2. Создание базового шаблона

Создадим базовый шаблон, который будет использоваться для всех страниц нашего приложения. Этот шаблон будет включать в себя общие части, такие как шапка, подвал и основная структура.

Файл: `resources/views/layouts/app.blade.php`

```blade
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title', 'My Laravel App')</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <header>
        <nav>
            <a href="{{ url('/') }}">Home</a>
            <a href="{{ url('/items') }}">Items</a>
        </nav>
    </header>
    
    <main>
        @yield('content')
    </main>
    
    <footer>
        <p>&copy; 2024 My Laravel App</p>
    </footer>
</body>
</html>
```

### 3. Наследование шаблона

Теперь создадим страницу, которая будет наследовать наш базовый шаблон.

Файл: `resources/views/items/index.blade.php`

```blade
@extends('layouts.app')

@section('title', 'Items List')

@section('content')
    <h1>Items List</h1>
    <a href="{{ url('/items/create') }}">Create New Item</a>
    <ul>
        @foreach($items as $item)
            <li>{{ $item->name }}</li>
        @endforeach
    </ul>
@endsection
```

## Создание CRUD Шаблонов

### 1. Список элементов (Read)

Начнем с создания шаблона для отображения списка элементов. Этот шаблон мы уже создали в предыдущем шаге (`resources/views/items/index.blade.php`).

### 2. Форма для создания нового элемента (Create)

Создадим шаблон для формы создания нового элемента.

Файл: `resources/views/items/create.blade.php`

```blade
@extends('layouts.app')

@section('title', 'Create Item')

@section('content')
    <h1>Create New Item</h1>
    <form action="{{ url('/items') }}" method="POST">
        @csrf
        <label for="name">Name:</label>
        <input type="text" id="name" name="name">
        <button type="submit">Create</button>
    </form>
@endsection
```

### 3. Форма для редактирования элемента (Update)

Создадим шаблон для формы редактирования существующего элемента.

Файл: `resources/views/items/edit.blade.php`

```blade
@extends('layouts.app')

@section('title', 'Edit Item')

@section('content')
    <h1>Edit Item</h1>
    <form action="{{ url('/items/' . $item->id) }}" method="POST">
        @csrf
        @method('PUT')
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" value="{{ $item->name }}">
        <button type="submit">Update</button>
    </form>
@endsection
```

### 4. Подтверждение удаления элемента (Delete)

Создадим шаблон для подтверждения удаления элемента.

Файл: `resources/views/items/delete.blade.php`

```blade
@extends('layouts.app')

@section('title', 'Delete Item')

@section('content')
    <h1>Delete Item</h1>
    <p>Are you sure you want to delete the item "{{ $item->name }}"?</p>
    <form action="{{ url('/items/' . $item->id) }}" method="POST">
        @csrf
        @method('DELETE')
        <button type="submit">Yes, Delete</button>
        <a href="{{ url('/items') }}">No, Cancel</a>
    </form>
@endsection
```

### 5. Контроллер

Для работы с этими шаблонами нам потребуется контроллер, который будет обрабатывать запросы. Создадим его с помощью Artisan-команды.

```sh
php artisan make:controller ItemController --resource
```

Этот контроллер будет содержать методы для обработки CRUD операций.

Файл: `app/Http/Controllers/ItemController.php`

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;

class ItemController extends Controller
{
    public function index()
    {
        $items = Item::all();
        return view('items.index', compact('items'));
    }

    public function create()
    {
        return view('items.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        Item::create($request->all());

        return redirect('/items');
    }

    public function edit($id)
    {
        $item = Item::findOrFail($id);
        return view('items.edit', compact('item'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $item = Item::findOrFail($id);
        $item->update($request->all());

        return redirect('/items');
    }

    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        return view('items.delete', compact('item'));
    }

    public function delete(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        $item->delete();

        return redirect('/items');
    }
}
```

### 6. Маршруты

Добавим маршруты для CRUD операций в файл `routes/web.php`.

```php
use App\Http\Controllers\ItemController;

Route::resource('items', ItemController::class);
```

## Заключение

Blade шаблоны в Laravel предоставляют мощные инструменты для создания гибких и повторно используемых компонентов пользовательского интерфейса. С помощью директив Blade и наследования шаблонов можно значительно упростить процесс разработки. Создание шаблонов для CRUD операций демонстрирует, как легко можно управлять ресурсами в Laravel с использованием Blade.

Этот подход позволяет разработчикам сосредоточиться на логике приложения и быстрее создавать качественные веб-приложения.


Методы HTTP запросов
GET - получение данных
POST - создание данных
PUT - обновление данных ['name' => 'new name']
DELETE - удаление данных

PATCH - частичное обновление данных ['name' => 'new name']
